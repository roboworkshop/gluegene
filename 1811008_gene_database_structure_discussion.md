# DNA Database discussion

## Questions to be addressed
- What are the common data fields of interest?
- Which things to store and in what way?
- What is the minimal required data?
- SQL vs NoSQL
    - SQL because we only store relational data.
- One database vs. multiple databases
    - Examples:
        - One each for DNA design/construction, one for constructed plasmids/strains, one for experiment data for constructed strains
        - or, one database for all these
    - Considerations/tradeoffs:
        - (Efficiency and consistent and less programmer work) vs. modularity/decoupling
            - Decision depends on coupling between tables.
- How best to store flexible key / value pair fields?
    - Table KeyValue_X with SeqID, KeyID, Value in Type X
    - Table Key with ID, Name, Description (perhaps)
    - Table KeyValue_Y with SeqID, KeyID, Value in Type Y

## Use Cases
- Enable searching through sequences not otherwise searchable
    - Details:
        - Either entire sequences, sequences with mismatches, subset sequences
        - Want to know quantitative redundancy of sequence
    - Reasons why can't do it now:
        - Queries are too slow
        - Can only query file metadata, not even sequence metadata

- Search across multiple sequence databases, not just NCBI
    - ~1,400 public DNA sequence databases
    - Unpublished JGI data
        - JGI doesn't make nr/nt equivalent & even if so annotations and metadata (e.g. geolocation data) may be different between entries 
    - Different databases emphasize different types of annotations, e.g. enzyme function vs. pathogenicity
- Phylogenetic tree / evolution of related sequences
    - Details:
        - What are the interesting positions in protein or DNA?
        - Which positions are conserved / variable? 

## Some details of current implementation
- Store full DNA sequence in addition to sequence hash

## Wish list
- protein and other complex sequence types
- binary DNA sequence format
- compressed DNA sequence format
    - Implement in Database, implementation is propably done.
        - https://dev.mysql.com/doc/refman/5.7/en/innodb-compression-background.html
- algorithms more scalable than BLAST?
    - BLAST is scalable, it can run on multiple servers.


